#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=${HEASERVER_REGISTRY_URL:-http://heaserver-registry:8080}
EOF

exec heaserver-trash-aws-s3 -f .hea-config.cfg -b ${HEASERVER_TRASH_AWS_S3_URL:-http://localhost:8080}
