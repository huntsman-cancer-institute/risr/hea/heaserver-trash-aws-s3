#!/usr/bin/env python3

from heaserver.trashawss3 import service
from heaserver.service.testcase import swaggerui
from integrationtests.heaserver.trashawss3integrationtest.testcase import db_store
from heaserver.service.testcase.testenv import MicroserviceContainerConfig
from heaserver.service.testcase.dockermongo import DockerMongoManager
from heaserver.service.testcase.awsdockermongo import S3WithDockerMongoManager
from heaserver.service.testcase.collection import CollectionKey
from heaserver.service.wstl import builder_factory
from heaobject.registry import Resource
from heaobject.user import NONE_USER
from heaobject.volume import AWSFileSystem, DEFAULT_FILE_SYSTEM
from heaobject.data import AWSS3FileObject
from aiohttp.web import get, post, put, delete, view, options

import logging

logging.basicConfig(level=logging.DEBUG)

db_store = {CollectionKey(name='components', db_manager_cls=DockerMongoManager): [{
        'id': '666f6f2d6261722d71757578',
        'created': None,
        'derived_by': None,
        'derived_from': [],
        'description': None,
        'display_name': 'Reximus',
        'invited': [],
        'modified': None,
        'name': 'reximus',
        'owner': NONE_USER,
        'shared_with': [],
        'source': None,
        'type': 'heaobject.registry.Component',
        'version': None,
        'base_url': 'http://localhost:8080',
        'resources': [{'type': 'heaobject.registry.Resource',
                       'resource_type_name': AWSS3FileObject.get_type_name(),
                       'base_path': 'volumes'}]
    }],
    CollectionKey(name='filesystems', db_manager_cls=DockerMongoManager): [{
        'id': '666f6f2d6261722d71757578',
        'created': None,
        'derived_by': None,
        'derived_from': [],
        'description': None,
        'display_name': 'Amazon Web Services',
        'invited': [],
        'modified': None,
        'name': 'amazon_web_services',
        'owner': NONE_USER,
        'shared_with': [],
        'source': None,
        'type': 'heaobject.volume.AWSFileSystem',
        'version': None
    }],
    CollectionKey(name='volumes', db_manager_cls=DockerMongoManager): [{
        'id': '666f6f2d6261722d71757578',
        'created': None,
        'derived_by': None,
        'derived_from': [],
        'description': None,
        'display_name': 'My Amazon Web Services',
        'invited': [],
        'modified': None,
        'name': 'amazon_web_services',
        'owner': NONE_USER,
        'shared_with': [],
        'source': None,
        'type': 'heaobject.volume.Volume',
        'version': None,
        'file_system_name': 'amazon_web_services',
        'file_system_type': 'heaobject.volume.AWSFileSystem',
        'credential_id': None  # Let boto3 try to find the user's credentials.
    }]}

HEASERVER_REGISTRY_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-registry:1.0.0'
HEASERVER_VOLUMES_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-volumes:1.0.0'
HEASERVER_BUCKETS_IMAGE = 'registry.gitlab.com/huntsman-cancer-institute/risr/hea/heaserver-buckets:1.0.0'

if __name__ == '__main__':
    volume_microservice = MicroserviceContainerConfig(image=HEASERVER_VOLUMES_IMAGE, port=8080, check_path='/volumes',
                                                db_manager_cls=DockerMongoManager,
                                                resources=[Resource(resource_type_name='heaobject.volume.Volume',
                                                                    base_path='volumes',
                                                                    file_system_name=DEFAULT_FILE_SYSTEM),
                                                           Resource(resource_type_name='heaobject.volume.FileSystem',
                                                                    base_path='filesystems',
                                                                    file_system_name=DEFAULT_FILE_SYSTEM)])
    bucket_microservice = MicroserviceContainerConfig(image=HEASERVER_BUCKETS_IMAGE, port=8080, check_path='/ping',
                                                db_manager_cls=DockerMongoManager,
                                                resources=[Resource(resource_type_name='heaobject.folder.Folder',
                                                                    base_path='volumes',
                                                                    file_system_type=str(AWSFileSystem),
                                                                    file_system_name=DEFAULT_FILE_SYSTEM)],
                                                env_vars={'HEA_MESSAGE_BROKER_ENABLED': 'false'})
    swaggerui.run('heaserver-trash-aws-s3', desktop_objects={k: v for k, v in db_store.items() if k.db_manager_cls is DockerMongoManager},
                  wstl_builder_factory=builder_factory(service.__package__),
                  routes=[(get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}', service.get_deleted_item),
                          (options, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}', service.get_deleted_item),
                          (options, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}/opener', service.get_trash_item_opener),
                          (get, '/volumes/{volume_id}/awss3trash/', service.get_all_deleted_items_all_buckets),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/', service.get_all_deleted_items),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3folders/{folder_id}/awss3trash/', service.get_deleted_items_in_folder),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}/restorer', service.restore_object),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}/deleter', service.permanently_delete_object),
                          (delete, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}', service.permanently_delete_object_with_delete),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trashemptier', service.do_empty_trash),
                          (get, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trashfolders/{trash_folder_id}/items/', service.get_items_in_trash_folder),
                          (view, '/volumes/{volume_id}/buckets/{bucket_id}/awss3trash/{id}/opener', service.get_trash_item_opener)],
                  registry_docker_image=HEASERVER_REGISTRY_IMAGE,
                  db_manager_cls=S3WithDockerMongoManager,
                  other_docker_images=[volume_microservice, bucket_microservice])
